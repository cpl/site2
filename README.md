This repository is for issues that I make about: 
- redirects and URLs (301s, renaming slugs;)
- content/information archetecture on CPL.ORG

(For outsiders, most of the discussion on these issues especially ones 
that I don't have sole responsibility over (which are most issues) 
ends up taking place in person, emails, Connectwise, an internal 
ticketing system, and are not reflected on here)

(I had attempted to encourage coworkers (specifically marketing) 
to use this but they prefered the other outlets and since the creation of this, 
we adopted Connectwise which is an issue tracking system. 

This repo still functions as a good long running list of tasks to
do or eventually discuss with the marketing team)

*Ask: Is this question about the technical layout/design of a page?* 

If no, or you're unsure, post it in here. 

Else, post in https://gitlab.com/cpl/tempera-nocopyrt/issues


===== 

Editing the Homepage
Homepage slideshow:

 What you need to start: Slideshow Image, Slideshow headline, and slideshow text

    Log in to admin panel
    Go to "Home Slides"
    Click "Add New
    Title: This is not visible and is more of an internal reference of what the slide is related to
    Body: 
        Use Heading 1 style for Headline
        Use Paragraph style for brief description/ slideshow text
    Link URL: Link to related page
    Slide Background Image: Image that appears behind the text and icon (1400x998)
    Save/ Publish / Update
    Refresh homepage and you can see the new slide added

Home Topics: 

To add a new home topic or to change what is on the homepage, follow the steps below.

What You Need to Start: Topic Background Image, Topic Icon, Link to Topic Page

    Log in to admin panel
    Go to "Home Topics"
    Click "Add New
    Title: Home Topic
    Topic Icon: Icon for topic (44x43)
    Link URL: Link to Topic Page
    Topic Background Image: Image that appears behind the text and icon (390x275)
    Save/ Publish / Update
    Refresh homepage and you can see the new topic added

